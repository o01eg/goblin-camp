Goblins camp is Dwarf Fortress-like game.
The code have been abandoned some years ago, but you can still build and run it in in Debian Wheezy
chroot container.

Please go to the [cmake-build](https://gitlab.com/dhyannataraj/goblin-camp/blob/cmake-build/README.md)
branch to get cmake-buildable version. You can find building instructions there

